#!/bin/bash

git update-index --assume-unchanged testserver/server.properties
git update-index --assume-unchanged testserver/spigot.yml
git update-index --assume-unchanged testserver/usercache.json
