package org.rootedinc.dbt.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.UUID;

/**
 * This awesome source file has been created by Hans Goor on 09/12/16.
 */
public class PlayerListener implements Listener {

    public static HashMap<UUID, Boolean> diamondTrollList = new HashMap<>();
    public static HashMap<UUID, Boolean> stickList = new HashMap<>();
    public static HashMap<UUID, Boolean> blockList = new HashMap<>();
    public static HashMap<UUID, Boolean> floorlavaList = new HashMap<>();

    @EventHandler
    public void onPlayerPickup(PlayerPickupItemEvent e) {
        Player p = e.getPlayer();
        if (diamondTrollList.get(p.getUniqueId())) {
            if (e.getItem().getItemStack().getType() == Material.DIAMOND) {
                Block b = p.getWorld().getBlockAt(new Location(p.getWorld(), p.getLocation().getBlockX(), p.getLocation().getBlockY() - 1, p.getLocation().getBlockZ()));
                b.setType(Material.LAVA);
            }
        }
    }

    @EventHandler
    public void onPlayerRightClickEntity(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        if (stickList.get(p.getUniqueId()) && (p.getInventory().getItemInMainHand().getType() == Material.STICK || p.getInventory().getItemInOffHand().getType() == Material.STICK)) {
            Entity target = e.getRightClicked();
            Vector v = target.getVelocity();
            v.setY(v.getY() + 1.5D);
            target.setVelocity(v);
        }
    }

    @EventHandler
    public void onPlayerRightClickBlock(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Block b = e.getClickedBlock();
        Location l = new Location(b.getWorld(), b.getLocation().getBlockX() + .5, b.getLocation().getBlockY(), b.getLocation().getBlockZ() + .5);

        if (blockList.get(p.getUniqueId()) && (p.getInventory().getItemInMainHand().getType() == Material.STICK || p.getInventory().getItemInOffHand().getType() == Material.STICK)) {
            Entity entity = b.getWorld().spawnEntity(l, EntityType.FALLING_BLOCK);
            Vector v = entity.getVelocity();
            v.setY(v.getY() + 1D);
            entity.setVelocity(v);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (floorlavaList.get(p.getUniqueId())) {
            Location l = p.getLocation();
            Block b = p.getWorld().getBlockAt(l.getBlockX(), l.getBlockY() - 1, l.getBlockZ());
            if (p.getLocation().getY() > 60 && b.getType() != Material.AIR && b.getType() != Material.BEDROCK) {
                b.setType(Material.LAVA);
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        diamondTrollList.put(p.getUniqueId(), false);
        stickList.put(p.getUniqueId(), false);
        blockList.put(p.getUniqueId(), false);
        floorlavaList.put(p.getUniqueId(), false);
    }

}
