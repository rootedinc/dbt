package org.rootedinc.dbt.commands;

import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.rootedinc.dbt.DBT;
import org.rootedinc.dbt.listeners.PlayerListener;

/**
 * This awesome source file has been created by Hans Goor on 09/12/16.
 */
public class CommandHandler implements CommandExecutor {

    private void printHelp(CommandSender sender) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            DBT.sendToSender(
                    p, "Help:\n" +
                            "/dbt creep <player>, spawns a creeper near a player.\n" +
                            "/dbt fly <player>, sends a player flying.\n" +
                            "/dbt diamondtroll <player> <enable/disable>, lava >:)\n" +
                            "/dbt stick <enable/disable>, just right click someone or something with a stick.\n" +
                            "/dbt tnt <player>, spawns a (fake) tnt block at someones feet.\n" +
                            "/dbt sethunger <player> <hunger>\n" +
                            "/dbt sethealth <player> <health>\n" +
                            "/dbt setsaturation <player> <saturation>\n" +
                            "/dbt achievement all/none, self explanatory\n" +
                            "/dbt enchant <all/none/enchantment, level>, also self-explanatory.\n" +
                            "/dbt floorlava <enable/disable>"
            );
        } else {
            DBT.sendToSender(sender, "Nope, you have no commands.");
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length < 1) {
            printHelp(sender);
            return false;
        }

        if (args[0].equalsIgnoreCase("help")) {
            printHelp(sender);
            return false;
        }

        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(!p.isOp()) { DBT.sendToSender(p, "These are only for admins ;P"); return false; }

            if(args[0].equalsIgnoreCase("creep")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "You have to specify a player.");
                    return false;
                } else {
                    Player target = Bukkit.getPlayer(args[1]);
                    target.getWorld().spawnEntity(target.getLocation(), EntityType.CREEPER);
                    DBT.sendToSender(p, "Success!");
                }
            }

            else if(args[0].equalsIgnoreCase("fly")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify a player.");
                    return false;
                } else {
                    Player target = Bukkit.getPlayer(args[1]);
                    Vector v = target.getVelocity();
                    v.setY(v.getY() + 1.5D);
                    target.setVelocity(v);
                }
            }

            else if(args[0].equalsIgnoreCase("diamondtroll")) {
                if(args.length < 3) {
                    DBT.sendToSender(p, "Please specify a player and enable/disable.");
                    return false;
                } else {
                    Player target = Bukkit.getPlayer(args[1]);
                    if(args[2].equalsIgnoreCase("enable")) {
                        PlayerListener.diamondTrollList.put(target.getUniqueId(), true);
                        DBT.sendToSender(p, "Enabled!");
                    } else if(args[2].equalsIgnoreCase("disable")) {
                        PlayerListener.diamondTrollList.put(target.getUniqueId(), false);
                        DBT.sendToSender(p, "Disabled!");
                    }
                }
            }

            else if(args[0].equalsIgnoreCase("stick")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify enable or disable.");
                    return false;
                } else {
                    if(args[1].equalsIgnoreCase("enable")) {
                        PlayerListener.stickList.put(p.getUniqueId(), true);
                        PlayerListener.blockList.put(p.getUniqueId(), true);
                        DBT.sendToSender(p, "Enabled!");
                    } else if(args[1].equalsIgnoreCase("disable")) {
                        PlayerListener.stickList.put(p.getUniqueId(), false);
                        PlayerListener.blockList.put(p.getUniqueId(), false);
                        DBT.sendToSender(p, "Disabled!");
                    }
                }
            }

            else if(args[0].equalsIgnoreCase("tnt")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify a player.");
                    return false;
                } else {
                    Player target = Bukkit.getPlayer(args[1]);
                    target.getWorld().spawnEntity(target.getLocation(), EntityType.PRIMED_TNT);
                }
            }

            else if(args[0].equalsIgnoreCase("sethunger")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify a player.");
                    return false;
                }

                Player target = Bukkit.getPlayer(args[1]);

                int food = 0;
                if(args.length == 3) {
                    food = Integer.parseInt(args[2]);
                }

                target.setFoodLevel(food);

                DBT.sendToSender(p, "Food set!");
            }

            else if(args[0].equalsIgnoreCase("sethealth")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify a player.");
                    return false;
                }

                Player target = Bukkit.getPlayer(args[1]);

                double health = 0D;
                if(args.length == 3) {
                    health = Double.parseDouble(args[2]);
                }

                target.setHealth(health);

                DBT.sendToSender(p, "Health set!");
            }

            else if(args[0].equalsIgnoreCase("setsaturation")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify a player.");
                    return false;
                }

                Player target = Bukkit.getPlayer(args[1]);

                float saturation = 0F;
                if(args.length == 3) {
                    saturation = Float.parseFloat(args[2]);
                }

                target.setSaturation(saturation);

                DBT.sendToSender(p, "Saturation set!");
            }

            else if(args[0].equalsIgnoreCase("achievement")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "All or none?");
                    return false;
                }

                if(args[1].equalsIgnoreCase("all")) {
                    for(Achievement a : Achievement.values()) {
                        p.awardAchievement(a);
                    }
                } else if(args[1].equalsIgnoreCase("none")) {
                    for(Achievement a : Achievement.values()) {
                        p.removeAchievement(a);
                    }
                }
            }

            else if(args[0].equalsIgnoreCase("enchant")) {
                if(args.length == 3) {
                    Enchantment e = Enchantment.getByName(args[1]);
                    int level = Integer.parseInt(args[2]);

                    p.getInventory().getItemInMainHand().addUnsafeEnchantment(e, level);
                } else if(args.length == 2) {
                    if (args[1].equalsIgnoreCase("all")) {
                        for (Enchantment e : Enchantment.values()) {
                            p.getInventory().getItemInMainHand().addUnsafeEnchantment(e, 1337);
                        }
                    } else if (args[1].equalsIgnoreCase("none")) {
                        for (Enchantment e : Enchantment.values()) {
                            p.getInventory().getItemInMainHand().removeEnchantment(e);
                        }
                    }
                } else {
                    DBT.sendToSender(p, "Please specify an enchantment and a level.");
                }
            }

            else if(args[0].equalsIgnoreCase("floorlava")) {
                if(args.length < 2) {
                    DBT.sendToSender(p, "Please specify enable/disable.");
                    return false;
                }

                if(args[1].equalsIgnoreCase("enable")) {
                    PlayerListener.floorlavaList.put(p.getUniqueId(), true);
                    DBT.sendToSender(p, "Enabled!");
                } else if(args[1].equalsIgnoreCase("disable")) {
                    PlayerListener.floorlavaList.put(p.getUniqueId(), false);
                    DBT.sendToSender(p, "Disabled!");
                }
            }

            else {
                sender.sendMessage(DBT.prefix + "Command not found!\n/dbt help for info.");
            }
        } else {
            DBT.sendToSender(sender, "This command is not for you.");
        }
        return false;
    }
}
