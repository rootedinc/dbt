package org.rootedinc.dbt;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.rootedinc.dbt.commands.CommandHandler;
import org.rootedinc.dbt.listeners.PlayerListener;

import java.util.logging.Level;

/**
 * This awesome source file has been created by Hans Goor on 09/12/16.
 */


public class DBT extends JavaPlugin{

    private static DBT instance;
    public DBT() {
        instance = this;
    }

    public static final String prefix = String.format(
            "%s[%sD%sB%sT%s] ",
            ChatColor.GRAY,
            ChatColor.GOLD,
            ChatColor.AQUA,
            ChatColor.GOLD,
            ChatColor.GRAY
    );

    public static void sendToSender(CommandSender sender, String message) {
        sender.sendMessage(prefix + message);
    }

    public void logToConsole(String message) {
        Bukkit.getLogger().log(Level.INFO, message);
    }

    public void onEnable() {
        CommandHandler exec = new CommandHandler();
        Bukkit.getPluginCommand("dbt").setExecutor(exec);

        Bukkit.getPluginManager().registerEvents(new PlayerListener(), instance);

        logToConsole("Plugin successfully loaded!");
    }

    public void onLoad() {
        for(Player p : Bukkit.getOnlinePlayers()) {
            PlayerListener.diamondTrollList.put(p.getUniqueId(), false);
            PlayerListener.stickList.put(p.getUniqueId(), false);
            PlayerListener.blockList.put(p.getUniqueId(), false);
            PlayerListener.floorlavaList.put(p.getUniqueId(), false);
        }
    }

    public void onDisable() {
        logToConsole("Plugin successfully unloaded!");
    }
}
